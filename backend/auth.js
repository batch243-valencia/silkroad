// import { sign, verify, decode, JsonWebTokenError } from "jsonwebtoken";
require("dotenv").config();
const jwt = require("jsonwebtoken");


const verifyIfSeller = async(req,res,next) => {
	console.log("isSellerVerified")
	//Verify If 
try {
	let token = await req.headers.authorization;
	// console.log(token);
	const userData =  await decode(token);
	// console.log(userData);
	(!userData.isSeller)?  res.status(401).send("Please Login As Seller"): next();
	} catch (error) {
	console.log(error.message)
}	
	
};
const getUserId = (req,res) => {
	console.log("getUserId")
	//Get User ID
	try {
	const userData = decode(req.headers.authorization);
	return userData.id	
	} catch (error) {
		console.log("GetuserID Catched")
		return null
	}
	
};


const createToken = (user) => {
	// Payload of JWT
	const data = {
		id: user._id,
		userName: user.userName,
		email: user.email,
		isSeller: user.isSeller,
	};
	// Generate a JSON web token using the jwt's sign method
	return jwt.sign(data, process.env.TOKEN_KEY, {});
};

const verify = (req, res, next) => {
	console.log("tokenVerified")
	let token = req.headers.authorization;
	try {
		// Validate the "token" using verify method, to decrypt the token using the secret code.

		if (!token) {
			return res.send("Authentication failed! No token provided");
		}

		token = token.replace("Bearer ", "");
		return jwt.verify(token, process.env.TOKEN_KEY, (error) => {
			if (error) {
				return res.send("Invalid Token");
			} else {
				next();
			}
		});
	} catch (error) {
		console.log(`Im in verify \n${error.message}`);
		res.send(error.message);
	}
};

const decode = (token) => {
	if (!token) {
		return null;
	} else {
		token = token.replace("Bearer ", "");
		// console.log (token)
		return jwt.verify(token, process.env.TOKEN_KEY, (error, data) => {
			if (error) {
				return null;
			} else {
				result = jwt.decode(token, { complete: true }).payload;
				console.log(result);
				return result
			}
		});
	}
};

module.exports = {getUserId, createToken, verify, decode, verifyIfSeller };
