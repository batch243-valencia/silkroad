const express= require('express')
const router = express.Router()
const {BucketModel, SkuModel} =require('../models/Products')
const {verifyIfSeller,verify} = require('../auth')
const {
  // Filters
  prodGetAll,
  prodGet, //Specific
  prodGetCategoryOf,
  prodGetTypeOf,
  prodSearch,
    // Seller
    bucketGetAllFromSeller,
    bucketGetFromSeller, //Specific
    bucketGetCategoryOfFromSeller,
    bucketGetTypeOfFromSeller,
    bucketProdSearch,
    // Sku
    skuGetAllFromSeller,
    skuGetFromSeller, //Specific
    skuGetCategoryOfFromSeller,
    skuGetTypeOfFromSeller, 
  /*****************************/
  // Create
  bucketCreate,
  skuCreate,
  //Update
  bucketUpdate,
  skuUpdate,
  // Archive
  bucketArchive,
  skuArchive,
  // Delete
  bucketDelete,
  skuDelete
 
  
} = require("../controllers/products");
// req.query.admin
// localhost:3000/?Active=true

/******************************
 Filters
*******************************/
    // bucketGetAll,
    router.get('/',prodGetAll)
    // bucketGet,/Specific
    router.get('/get/:search',prodGet)
    // bucketGetCategoryOf,
    router.get('/category/:category',prodGetCategoryOf)
    // bucketGetTypeOf,
    router.get('/type/:type',prodGetTypeOf)
    // bucketSearch
    router.get('/search/',prodSearch)
    router.get('/search/:search',prodSearch)



    //bucket
        // bucketGetAllFromSeller,
        router.get('/seller/:id',bucketGetAllFromSeller)
        // bucketGet,//Specific
        
        router.get('/admin/seller/:id/prodId/:prodId',bucketGetFromSeller)
        router.get('/seller/:id/prodId/:prodId',bucketGetFromSeller)
        // bucketGetCategoryOfFromSeller,
        router.get('/seller/:id/category/:category',bucketGetCategoryOfFromSeller)
        // bucketGetTypeOfFromSeller,
        router.get('/seller/:id/type/:type', bucketGetTypeOfFromSeller)
        // bucketProdSearch
        router.get('seller/:id/search/', bucketProdSearch)
        router.get('seller/:id/search/:search', bucketProdSearch)
    //sku
        // // skuGetAllFromSeller,
        router.get('/skus/:seller/:prodId/',skuGetAllFromSeller)
        skuGetFromSeller,//Specific
        router.get('/sku/:seller/:prodId/:skuId',skuGetFromSeller)

/******************************
 Create
*******************************/
    // bucketCreate,
    router.post('/seller/:id/create',  bucketCreate,)
    // skuCreate,
    router.post('/sku/:bucketId/create',skuCreate)
/******************************
 Update
*******************************/   
    // bucketUpdate,
    router.patch('/seller/update/:prodId',verify,verifyIfSeller,bucketUpdate)
    // skuUpdate,
    router.patch('/sku/update/:skuId',verifyIfSeller,skuUpdate)
/******************************
 Archive
*******************************/
    // bucketArchive,
    // router.patch('/seller/archive',verify,verifyIfSeller,bucketArchive)
    // skuArchive,
    router.patch('/sku/archive',verifyIfSeller,skuArchive)
/******************************
 Delete
*******************************/
    // bucketDelete,
    router.delete('/seller/:userId/delete/:prodId/',verify,verifyIfSeller,bucketDelete)
    // skuDelete    
    router.delete('/sku/:prodId/delete/:skuId',verifyIfSeller,skuDelete)

    

module.exports = router