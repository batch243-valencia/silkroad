const express = require("express");
require("dotenv").config();
const router = express.Router();
const auth = require("../auth");

const {
	
	loginUser,
	registerUser,
} = require("../controllers/users");


const { createToken, verify, decode } = "../auth";

router.post("/login", loginUser);
// router.get("/profileDetails", profileDetails);
router.post("/register", registerUser);
router.post("/register/:isSeller", registerUser);


router.post("/updateCart/")
router.post("/Checkout/", )
// router.patch("/updateRole/:userId", auth.verify, updateRole);
// GET all Users
// router.get('/', getUsers)

// // GET a single Users
// router.get('/:id', getUsers)

// // POST a new Users
// router.post('/', createUsers)

// // DELETE a Users
// router.delete('/:id', deleteUsers)

// // UPDATE a Users
// router.patch('/:id', updateUsers)

module.exports = router;
