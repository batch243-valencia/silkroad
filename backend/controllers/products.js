const {model, now} = require("mongoose");
const util = require('util')
const {BucketModel, SkuModel} = require("../models/Products");
const {decode,getUserId} = require("../auth");
const { isatty } = require("tty");
const { query } = require("express");
const { search } = require("../routes/productRoutes");
const { error } = require("console");
const { OrderModel } = require("../models/Orders");
// bucketGetAll, //Excluding Inactive
// try {
  // const buckets = await BucketModel.find().populate("sku").where('sku[0].color').equals('green')

  //   const buckets = await BucketModel.find({
//       sku:{$in:[{isActive:true}]}
//   }).populate("sku")
// res.send(buckets)
// } catch (error) {
// return res.json ({error: error.message})  
// }

/******************************
 Filters
 *******************************/
const prodGetAll = async (req, res) => {
  const filter={}
try{
  const items = await getBucketFiltered(req,res,filter)
  return res.send(items)
  } catch (error) {
    return res.json({error:error.message})
  }
};
  // prodGet,//Specific
const prodGet = async (req, res) => {
  try {
  const {search} = req.params
  const filter = {   
      $or:
      [
        {_id:search},
        {bucket:search},
        {type:search},
        {category:search}
      ]
  }
  const bucketResult = await BucketModel.findOne(filter).populate("sku")
  if (Object.keys(bucketResult).length===0) return res.send("Sorry Product Not Found!")
  res.send(bucketResult)
  } catch (error) {
     return res.json({error: error.message})
  }
  
}
  // prodGetCategoryOf,
const prodGetCategoryOf = async (req, res, next) => {
  const {category} = req.params
  const filter = {   
      category:category
  }
try {
    const items = await getBucketFiltered(req,res,filter)
  if (Object.keys(items).length===0) return res.send("Sorry Product Not Found!")
  res.send(items)
} catch (error) {
  return res.json({error: error.message})
}
};
  // prodGetTypeOf,
const prodGetTypeOf = async (req, res, next) => {
  const {type} = req.params
  const filter = {   
      type:type
  }
  try {
    const items = await getBucketFiltered(req,res,filter)
    if (Object.keys(items).length===0) return res.send("Sorry Product Not Found!")
    res.send(items)      
  } catch (error) {
    return res.json({error: error.message})
  }
};
  // prodSearchs
const prodSearch = async (req, res, next) => {
  let search = null
  if (req.query)search = await req.query.search
  if (req.params.search)search = await req.params.search
  const filter = {   
      $or:
      [
        {bucket:search},
        {type:search},
        {category:search},
        // {"sku": {$elemMatch:  {"color": "canvas", size: ""}}},
      ]
  }
  try {
  console.log(util.inspect(filter, false, null, true /* enable colors */))
  const items = await getBucketFiltered(req,res,filter)
  if (Object.keys(items).length===0) return res.send("Sorry Product Not Found!")
  res.send(items)      
  } catch (error) {
    return res.json({error: error.message})
  }
  
};

//Filters Seller *********************************
  // bucketGetAllFromSeller,
const bucketGetAllFromSeller = async (req, res, next) => {
  const {id} = req.params

  const userId = getUserId(req,res);
  // console.log(userId);
  const isOwner = (id===userId)
  console.log("isOwner:"+isOwner)
  const filter = {
    seller:id
  }
  try {
  const items = await getSellersItems(req,res,filter,isOwner)
  // console.log(util.inspect(items, false, null, true /* enable colors */))
  if (Object.keys(items).length===0) return res.send("Sorry Product Not Found or Unavailable!")
  console.log(items);
  res.send(items)
  } catch (error) {
    return res.json({error: error.message})
  }
};
  // bucketGetFromSeller,//Specific
const bucketGetFromSeller = async (req, res, next) => {
  console.log("bucketGetFromSeller")
  const {id,prodId}= req.params
  const userId = getUserId(req,res);
  // console.log(userId);
  const isOwner = (id===userId)
  // console.log("isOwner:"+isOwner)
  const filter = {
    seller:id,
    _id:prodId,
    
  }
  console.log(filter)
  try {
  const items = await getSellersItems(req,res,filter,isOwner)
  console.log(util.inspect(items, false, null, true /* enable colors */))
  if (Object.keys(items).length===0) return res.send("Sorry Product Not Found or Unavailable!")
  // console.log(items);
  res.send(items)
  } catch (error) {
    return res.json({error: error.message})
  }
};
  // bucketGetCategoryOfFromSeller,    
const bucketGetCategoryOfFromSeller = async (req, res, next) => {
  const {id,category}= req.params
  const userId = getUserId(req,res);
  // console.log(userId);
  const isOwner = (id===userId)
  // console.log("isOwner:"+isOwner)
  const filter = {
    seller:id,
    category:category
  }
  try {
  const items = await getSellersItems(req,res,filter,isOwner)
  // console.log(util.inspect(items, false, null, true /* enable colors */))
  if (Object.keys(items).length===0) return res.send("Sorry Product Not Found or Unavailable!")
  // console.log(items);
  res.send(items)
  } catch (error) {
    return res.json({error: error.message})
  }
};
  // bucketGetTypeOfFromSeller,
const bucketGetTypeOfFromSeller = async (req, res, next) => {
  const {id,type}= req.params
  const userId = getUserId(req,res);
  // console.log(userId);
  const isOwner = (id===userId)
  // console.log("isOwner:"+isOwner)
  const filter = {
    seller:id,
    type:type
  }
  try {
  const items = await getSellersItems(req,res,filter,isOwner)
  // await console.log(util.inspect(items, false, null, true /* enable colors */))
  if (Object.keys(items).length===0) return res.send("Sorry Product Not Found or Unavailable!")
  // console.log(items);
  res.send(items)
  } catch (error) {
    return res.json({error: error.message})
  }
};

const bucketProdSearch = async(req,res)=>{
  console.log("bucketProdSearch")
  let search = null
  if (req.query)search = await req.query.search
  if (req.params.search)search = await req.params.search
  const {id} = req.params
  const userId = getUserId(req,res);
  const isOwner = (id===userId)
  const filter = {   
    $and: [
      {_id:id},
    {$or:
      [
        {bucket:search},
        {type:search},
        {category:search},
        // {"sku": {$elemMatch:  {"color": "canvas", size: ""}}},
      ]
    }
    ]
  }
  try {
  console.log(util.inspect(filter, false, null, true /* enable colors */))
  const items = await getBucketFiltered(req,res,filter)
  if (Object.keys(items).length===0) return res.send("Sorry Product Not Found!")
  res.send(items)      
  } catch (error) {
    return res.json({error: error.message})
  }
  
}

// Filter Sku *********************************
  // skuGetAllFromSeller,   
const skuGetAllFromSeller = async (req, res, next) => {
console.log(req.params.test)

const {seller,prodId}= await req.params

  const userId = await getUserId(req,res);
  console.log(userId);
  console.log(seller,userId)
  const isOwner = (seller===userId)
  console.log("isOwner:"+isOwner)
  const filter = {
    seller:seller,
    _id:prodId
  }
 const buckets = await BucketModel.find(filter).populate("sku")
//  await console.log(util.inspect(buckets, false, null, true /* enable colors */))
  let newSet = new Set();
  try {
    buckets.forEach(bucket => {
      bucket.sku.forEach(sku => {
        newSet.add(sku)
        // if(isOwner) newSet.add(sku)
        // // console.log(isOwner)
        // if(!isOwner && sku.isActive===true) newSet.add(sku)
      });
    });
    if (newSet.size===0) return res.send("Sorry Product Not Found or Unavailable!")
    // console.log(Array.from(newSet))
    // return (Array.from(newSet))
  return res.send(Array.from(newSet))
  } catch (error) {
    return res.send({error: error.message})
  }
    
   
};
  // skuGetFromSeller,//Specific
const skuGetFromSeller = async (req, res, next) => {
console.log("skuGetFromSeller")
// const {seller,prodId,skuId}= req.params
//   const userId = getUserId(req,res);
//   // console.log(userId);
//   const isOwner = (seller===userId)
//   console.log("isOwner:"+isOwner)
//   const filter = {
//     seller:seller,
//     _id:prodId,
//   }

//    const buckets = await BucketModel.find(filter).populate("sku")
// //  await console.log(util.inspect(buckets, false, null, true /* enable colors */))
//   let newSet = new Set();
//   try {
//     buckets.forEach(bucket => {
//       bucket.sku.forEach(sku => {
//         if(isOwner) newSet.add(sku)
//         if(!isOwner && sku.isActive===true) newSet.add(sku)
//       });
//     });
//     if (newSet.size===0) return res.send("Sorry Product Not Found or Unavailable!")
//     // console.log(Array.from(newSet))
//     // return (Array.from(newSet))
//   return res.send(Array.from(newSet))
//   } catch (error) {
//     return res.send({error: error.message})
//   }







//  const buckets = await BucketModel.find(filter).populate("sku")
//  let newSet = new Set();
// //  await console.log(util.inspect(buckets, false, null, true /* enable colors */))
//   try {
//     buckets.forEach(bucket => {
//       bucket.sku.forEach(sku => {
//         if(isOwner && sku._id==skuId) newSet.add(sku)
//         if(!isOwner && sku.isActive===true && sku._id==skuId) newSet.add(sku)
//       });
//     });

//     if (newSet.size===0) return res.send("Sorry Product Not Found or Unavailable!")
//     // return (Array.from(newSet))
//   return res.send(newSet)
//   } catch (error) {
//     return res.send({error: error.message})
//   }
    
   
};

  // bucketCreate,
const bucketCreate = async (req, res, next) => {
  const {id: UserId} = decode(req.headers.authorization);
  // console.log(`${UserId} ${req.params.id}`)
  if(req.params.id !== UserId) return res.status(403).send("Cant create A product for this user.")
  try {
    console.log("productTry")

const {bucket, productName, description, category, type, materials} = req.body;
const Bucket = {
      seller: UserId,
      bucket,
      productName,
      description,
      category,
      type,
      materials
    };
    const newBucket = await BucketModel.create(Bucket);
    return res.status(201).json(newBucket);
    next();
  } catch (error) {
  console.error(error);
  return res.status(500).json({ message: "Failed to create new bucket"});
  }
};

  // skuCreate,
const skuCreate = async (req, res, next) => {
  console.log("skuCreate")
  const userId = getUserId(req,res)
  // console.log(`userID: ${userId}`)
  const {bucketId} = req.params;
  // console.log("bucketID:"+bucketId)

  const Sku = {
    seller:userId,
    bucket:bucketId,
    price: req.body?.price,
    stocks: req.body
      ?.stocks,
    color: req.body
      ?.color,
    size: req.body
      ?.size,
    weight: req.body
      ?.weight
  };
  const bucket = await BucketModel.findOne({_id : bucketId,});
  try {
    // throw new Error("duplicate")
    //Check for Duplicate/s
    const Dup = await isDuplicate(bucketId,Sku.color,Sku.size)
    if (Dup) return res.status(401).send("error Duplicate SKU")
    // Guard Clause NO Duplicate Beyond THIS POINT↓ 
    const newSku = await SkuModel.create(Sku)
    const newSkuID= newSku._id
    bucket.sku.push(newSkuID);
    const newBucket = await bucket.populate("sku");  
    const savedBucket = await newBucket.save();
    res.status(201).json(savedBucket);;
    next();
  } catch (error) {
    console.error(error)
    res.status(400).send(error.message);
  }
}

  // bucketUpdate,
const bucketUpdate = async (req, res, next) => {
  console.log("bucketUpdate")
const userId = getUserId(req,res)
const {prodId} = req.params
  try {
  const seller  = await BucketModel.findById(prodId)

if (userId!=seller?.seller)return res.send("Error Cant Update this Product")
  const updates={
    "bucket":req.body?.bucket,
    "productName":req.body?.productName,
    "description":req.body?.description,
    "category":req.body?.category,
    "type":req.body?.type,
    "materials":req.body?.materials
  }
  console.log(updates)
const updatedItem = await BucketModel.findByIdAndUpdate(prodId,updates,{ new: true }).then(result=>{console.log(result)
  res.status(200).send(result)    
})
  } catch (error) {
    res.status(400).send(error.message)
  }
  
};
  // skuUpdate
const skuUpdate = async (req, res, next) => {
  console.log("skuUpdate")
const userId = getUserId(req,res)
const {skuId} = req.params
// console.log("skuId"+skuId)
  try {
  const sku  = await SkuModel.findById(skuId,{bucket:1})
  // console.log("sku:"+sku)
  const bucket  = await BucketModel.findById(sku?.bucket,{seller:1})
  if (userId!=bucket?.seller)return res.send("Error Cant Update this Product")
  // console.log("bucket:"+bucket)
  let updates=await {
    "price":req.body?.price,
    "stocks":req.body?.stocks,
    "color":req.body?.color,
    "size":req.body?.size,
    "weight":req.body?.weight,
    "isActive":req.body?.isActive,
  }
 await (!updates.isActive)
    ?updates = await {...updates, archivedDate:new Date()}
    :updates = await {...updates, archivedDate:null}

  const updatedItem = await SkuModel.findByIdAndUpdate(skuId,updates,{ new: true })
  console.log(updatedItem)
  res.send(updatedItem)  
  } catch (error) {
    res.send(error.message)
  }
};
/*************
    bucketArchive,
    *************/
const bucketArchive = async (req, res, next) => {
  console.log("bucketUpdate")
  const userId = getUserId(req,res)
  // console.log("prodId:"+prodId)
  const {
    prodId,
    isActive
  } = req.query;

  const updates={
    "isActive": isActive
  }  

  console.log("test")
  const updatedItem = await SkuModel.findByIdAndUpdate(skuId,updates,{ new: true })
  res.send(updatedItem)
};


/*************
    skuArchive,;p
    *************/
const skuArchive = async (req, res, next) => {
  console.log("bucketUpdate")
  const userId = getUserId(req,res)
  // console.log("prodId:"+prodId)
  const {
    skuId,
    isActive
  } = req.query;

  const updates={
    "isActive": isActive
  }  
  console.log("test")
  const updatedItem = await SkuModel.findByIdAndUpdate(skuId,updates,{ new: true })
  res.send(updatedItem)

};
const bucketDelete = async (req, res, next) => {
  const {userId,prodId} = req.params
  // console.log(prodId)
  if (userId!=userId)return 
  if (userId === userId){
  const buckets = await BucketModel.findById(prodId)
  // console.log(buckets)
  // let bucketArr={}
  // console.log(prodId)
 const bucketResult = await buckets.sku.map(
    sku=>{
      console.log(sku._id.toString())
      
    // bucketArr= {...{"_id":sku._id}}
  skuItems= SkuModel.findByIdAndDelete(sku._id.toString())
    }
  )                                                                               
  return res.send( bucketResult);

  };
}
/*************
    skuDelete
    *************/
const skuDelete = async (req, res, next) => {};
/************************************************/
/*                  UTILITY                     */
/************************************************/
const isDuplicate = async(bucketId,color,size)=>{
  console.log("isDuplicate")
  const field = await{
    "bucket": bucketId,
    "color": color,
    "size": size
  }
  console.log(field);
  const Duplicate =  await SkuModel.findOne(field);
  console.log("Duplicate Result:"+Duplicate)
  return Duplicate
}

const getBucketFiltered = async(req,res,filter,isOwner)=>{
  const buckets = await BucketModel.find(filter).populate("sku")
  let newSet = new Set();
  try {
    buckets.forEach(bucket => {
      bucket.sku.forEach(sku => {
        
        if(isOwner) newSet.add(buckets)
        if(!isOwner && sku.isActive===true && sku.stocks>0) newSet.add(buckets)
      });
    });
    // console.log(Array.from(newSet))
    // return (Array.from(newSet))
  return (Array.from(newSet))
  } catch (error) {
    return res.send({error: error.message})
  }
}


const getSellersItems= async(req,res,filter,isOwner)=>{
const buckets = await BucketModel.find(filter).populate("sku")
  let newSet = new Set();
  // let arr = []
  try {
  //   buckets.forEach(bucket => {
  //     bucket.sku.forEach(sku => {
  //       if(isOwner) newSet.add(buckets)
  //       if(!isOwner && sku.isActive===true) newSet.add(buckets)
  //     });
  //   });
  //   console.log(Array.from(newSet))
  //   return (Array.from(newSet))
  // return (Array.from(newSet))
  return buckets
  } catch (error) {
    return res.send({error: error.message})
  }
}

const getSkuItems = async (req,res,filter,isOwner)=>{
  const skus = await SkuModel.find(filter)
  let newSet = new Set();
  console.log(isOwner)
  try {
    skus.forEach(sku => {
      
      if (isOwner) newSet.add(sku)
      if(!isOwner&& sku.isActive===false) newSet.add(sku)
    });
    return (Array.from(newSet))
  } catch (error) { 
    return res.send({error: error.message})
  }
}


  function removeFunction(myObjects, prop, valu) {
      var newArray = myObjects.filter(function (val) {
          return val[prop] !== valu;
      });
      return newArray;
  }


  function createTshirt(){

  }

module.exports = {
    async createOrder(req, res) {
        const {user_id, items, totalAmount} = req.body;

        const existingOrder = await OrderModel.findOne({user_id});
        if (!existingOrder) {
            try {
                const order = await OrderModel.create(req.body);
                res.status(201).json(order);
            } catch (error) {
                res.status(400).json({ message: error.message });
            }
        } else {
            try {
                const updatedOrder = await OrderModel.findOneAndUpdate(
                    { user_id },
                    { $push: { items } },
                    { new: true }
                );
                res.status(201).json(updatedOrder);
            } catch (error) {
                res.status(400).json({ message: error.message });
            }
        }
    }
};
//     bucketGetAllActiveFromSeller,
//     *************/
// const bucketGetAllActiveFromSeller = async (req, res, next) => { 

//   const buckets = await BucketModel.find(
//   ).populate("sku")
//   let newSet = new Set();
//   try {
//     buckets.forEach(sku => {
//       sku.sku.forEach(element => {
//         if (sku.id="", element.isActive===true && element.stocks>0) newSet.add(sku) 
//       });
//     });
//   return res.send(Array.from(newSet))
//   } catch (error) {
//     return res.send(json({error: error.message}))
//   } 
// };
module.exports = {
  // Filters
  prodGetAll,
  prodGet, //Specific
  prodGetCategoryOf,
  prodGetTypeOf,
  prodSearch,
    // Seller
    bucketGetAllFromSeller,
    bucketGetFromSeller, //Specific
    bucketGetCategoryOfFromSeller,
    bucketGetTypeOfFromSeller,
    bucketProdSearch,
    // Sku
    skuGetAllFromSeller,
    skuGetFromSeller, //Specific
    // skuGetCategoryOfFromSeller,
    // skuGetTypeOfFromSeller, 
  /*****************************/
  // Create
  bucketCreate,
  skuCreate,
  //Update
  bucketUpdate,
  skuUpdate,
  // Archive
  bucketArchive,
  skuArchive,
  // Delete
  bucketDelete,
  skuDelete  
};
