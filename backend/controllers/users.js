const { CartModel, UsersModel, AddressModel } = require("../models/Users");
const { OrderModel } = require("../models/Orders")
const bcrypt = require("bcrypt");
const auth = require("../auth");
const mongoose = require("mongoose");
const { json } = require("express");
/****************
	register user
****************/
const createBilling = async (req, res) => {
	const billingAddress = {
		street,
		district,
		barangay,
		cityOrMunicipality,
		province,
		zip,
		country
	} = req.body.billingAddress
	try {
		return await AddressModel.create(billingAddress);
	} catch (error) {
		return json(error.message)
	}
}
const createShipping = async (req, res) => {
	const shippingAddress = {
		street=req?.body?.shippingAddress?.street,
		district=req.body?.shippingAddress?.district,
		barangay=req.body?.shippingAddress?.barangay,
		cityOrMunicipality=req.body?.shippingAddress?.cityOrMunicipality,
		province=req.body?.shippingAddress?.province,
		zip=req.body?.shippingAddress?.zip,
		country=req.body?.shippingAddress?.country
	} = req?.body?.shippingAddress ?? {}
	try {
		return await AddressModel.create(shippingAddress)
	} catch (error) {
		console.log(error.message)
	}
}
/****************
	Register user
****************/
const registerUser = async (req, res) => {
	// Initialize Users information
	const userInput = {
		userName,
		password,
		email,
		contactNo,
		isSeller,
		passwordRetrieval,
		firstName,
		middleName,
		lastName,
		// billingAddress:billingAddress._id,
		// ShippingAddress:shippingAddress._id
	} = req.body;
	// JUST FOR FUN! this is "unnecessary"
	// if req.body.isSeller || req.params.isSeller = (true)
	// set userInput.isSeller = (true)
	(userInput?.isSeller || req.params?.isSeller === 'true')
		? userInput.isSeller = true
		: userInput.isSeller = false
	// Try Creating Billing/Shipping Address
	// *****!IMPORTANT****
	// IF Fail DELETE Created Billing/Shipping Address!!!
	// *******************
	try {
		//Invoke a function to check if Address/"ES" are filledUp
		const billingAddress = await createBilling(req, res)
		const shippingAddress = await createShipping(req, res)
		// if AddressES are NOT null invoke Address to UserInput.(ADDRESS)
		userInput.billingAddress = (billingAddress) ? billingAddress._id : null
		userInput.shippingAddress = (shippingAddress) ? shippingAddress._id : null
		// Encrypt Password
		userInput.password = bcrypt.hashSync(password, 10)
		const newUser = await UsersModel.create(userInput)
		return res.status(200).send(newUser);
	} catch (error) {
		if (error.message.indexOf("11000") != -1) res.status(403).send(false);
		// CLEAN UP!!! Removed Created Address When Failed DI ko pa alam as of now papano
		// Validate muna Bago mag Create na di gumagamit ng FIND kala ko
		// pwede na yung UNIQ as is to catch error for duplicate entry..........
		await AddressModel.findByIdAndDelete(userInput?.billingAddress?._id)
		await AddressModel.findByIdAndDelete(userInput && userInput?.shippingAddress?._id)
		res.status(400)
		return false
	}
};
/****************
	Login user
****************/



const loginUser = async (req, res, next) => {
	const { userInput, password } = req.body;
	try {
		// User Input can be Username or Email Address
		// Same with facebook,steam etc
		// Check if !(empty/undefinrsed) else return status(400)Bad request
		if ((!userInput || !password)) return res.status(400).send("All input is required");
		// Get User Information
		let user = await findUser_UsingUserInput(userInput)
		// Validate Passsword
		if (!user) return res.status(400).send("ERROR")
		const isPasswordCorrect = bcrypt.compareSync(password, user.password)
		// Validate if User and password Are correct
		if (!user || !isPasswordCorrect) return res.status(400).send("Credentials missmatch")
		//Login As seller = default FALSE
		const token = auth.createToken(user);
		user.password="";
		// delete user.password;
		res.status(200).send({ "user": user, "token": token })
		// res.status(200).send(`${token} 

		// ${user}`);
		// console.log({ token });
	} catch (err) {
		console.log(err);
		return res.status(400).send("These credentials do not match our records");
	}
};

// FIND "User" using userInput
// Look if Email Match
// Look if Username Match
const findUser_UsingUserInput = async (userInput) => {
	return await UsersModel.findOne({
		$or: [
			{
				email: userInput,
			},
			{
				userName: userInput,
			},
			{
				contactNo: userInput,
			},
		],
	},
	{
		userName:1,
		password:1,
		email:1,
		contactNo:1,
		isSeller:1
	});
};


const addtoCart = async (req, res) => {

	const { user_id, product_id, sku_id, quantity, price } = req.body;
	const items = {
		items: {
			product_id,
			sku_id,
			quantity,
			price
		}
	}
	const OrderToBeAdded = {
		user_id,
		items: {
			product_id,
			sku_id,
			quantity,
			price
		},
		totalAmount: totalAmount + (price * quantity)
	}
	const existingOrder = await OrderModel.findOne({ user_id });
	if (!existingOrder) {
		try {
			const order = await OrderModel.create(OrderToBeAdded);
			res.status(201).json(order);
		} catch (error) {
			res.status(400).json({ message: error.message });
		}
	} else {
		try {
			const updatedOrder = await OrderModel.findOneAndUpdate(
				{ OrderToBeAdded },
				{ $push: { items } },
				{ new: true }
			);
			res.status(201).json(updatedOrder);
		} catch (error) {
			res.status(400).json({ message: error.message });
		}
	}
}

const updateCart = async (newCart, addItem=true) => {

	const {id} = decode(req.headers.authorization);
    try {
        const cart = await CartModel.findOne({ user_id: id});
        if (addItem) {
            cart.items.push(newCart);
        } else {
            cart.items = cart.items.filter(item => item !== newCart);
        }
        await cart.save();
        return cart;
    } catch (error) {
        console.log(error);
    }
}


// const view_Cart = async (req,res,next)=>{

// const ifCartExist = Ordermo

// user_id:"String",
//     cartName:"String",
//     items:[{
//         product_id:"String",
//         sku_id:"String",
//         quantity:"Number",
//         price:"Number"
//     }]		




// const newCart={
// 	user_id,
// 	items:[{
// 		product_id,
// 		sku_id,
// 		quantity,
// 		price
// 	}],
// 	totalAmount
// 	}=req.body

// const test= OrderModel.create(newCart)

module.exports = { registerUser, loginUser, updateCart };