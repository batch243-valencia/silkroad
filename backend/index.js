require("dotenv").config(); 
const express = require("express");
const app = express();


/**********************
    Connect to Database
**********************/
const {PORT, MONGODBURI} = process.env;

app.listen(PORT,()=>console.log(`The server started on port ${PORT}`))

const mongoose = require("mongoose");
const config= {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	}
mongoose.set({ strictQuery: true})
mongoose.connect(MONGODBURI, config)
const DB = mongoose.connection;
DB.on("open", () =>
    console.log(`listening for requests on port localhost:${PORT}`))
.on("close", () =>
    console.log("You are disconencted to Mongo"))
.on("error", (err) => console.log(err));
DB.once("open", () =>
    console.log("Now Connected to MongoDb Atlas"))


// middleware
const cors = require("cors");
app.use(cors());
app.use(express());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use((req, res, next) => {
	console.log(req.path, req.method);
	next();
});



// **********************
// ********ROUTES********
// **********************
// const userRoutes = require("./routes/userRoutes");
// app.use("/user", userRoutes);


const userRoutes = require("./routes/userRoutes");
app.use("/user", userRoutes);


const productRoutes = require("./routes/productRoutes");
app.use('/products',productRoutes)

// const cartRoutes = require("./routes/cartRoutes");
// app.use("/cart", cartRoutes);
