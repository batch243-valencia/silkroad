const  {Schema,  model, default: mongoose} = require('mongoose');

const BucketSchema = new Schema({
	seller:
		{type:
			mongoose.Types.ObjectId,
			ref:"users"},
	bucket:
		{
		type: "String",
 		unique: true,
		 min: [3,"minimum or 3 Characters"],
 		// required: [true, "Please add baseSku"],
	},
	productName: {
		type: "String",
		unique: [true, "Duplicate Product Name, Think Different"],
		min: [3,"minimum or 3 Characters"],
		// required: [true, "{Please Add a Product Name"],		
	},
	description: [{
		min: [3,"minimum or 3 Characters"],
		type: "String",
	}],
	category: {
		type: "String",
		lowercase: true,
		// required:true,
	},
	type: {
		type: "String",
		lowercase: true,
		// required:true,
	},
	materials: [{
		type: "String"
	}],
	rating:{
		type:"Number"
	},
	sku: [{type: mongoose.Types.ObjectId,ref:"sku"}]
})

const SkuSchema = new Schema({
        price: {
		type: "Number",
		},
		stocks: {
			type: "Number",
			default: 0,
		},
		color: {
			type: "String"
		},
		size: {
			type: "string"
		},
		weight: {
			type: "String"
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: Date.now
		},
		archivedDate: {
			type: Date,
			default: ""
		},
		bucket: {type: mongoose.Types.ObjectId,ref:"bucket"}
	}
	
)
const BucketModel =model("bucket",BucketSchema);
const SkuModel =model("sku",SkuSchema);
module.exports = {BucketModel,SkuModel}