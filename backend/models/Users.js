	const {model,Schema} = require("mongoose");
	const mongoose = require("mongoose");
	const addressSchema = new Schema({
		user_id:
			{
			type:mongoose.SchemaTypes.ObjectId,
			ref:'users'},
		street:
			{
			type: "String",
				required: [
					true,
					"Please input your Unit Number + House/Building/Street Number",
				],
			lowercase:true
			},
		district:
			{type:"String", lowercase:true},
		barangay:
			{type:"String", lowercase:true},
		cityOrMunicipality:
			{
			type:"String", lowercase:true,
			required: [true, "Please input your City or Municipality"],
		},
		province:
			{type:"String", lowercase:true},
		zip:
			{type:"String", lowercase:true},
		country:
			{
			type: "String",
			lowercase:true,
			required: true,
		}
		
	})
	const usersSchema = new Schema({
		userName:
		{
			type: "String",
			unique: true,
			required: true,
		},
		password:
		{
			type: "String",
			required: true,
		},
		email:
		{
			type: "String",
			unique: true,
			required: true,
		},
		contactNo:
		{
			type: "String",
			required: true,
			unique: true,
		},
		isSeller:
		{
			type: Boolean,
			required: true,
			default: false,
		},
		passwordRetrieval:
			{
			question1:
				{type: "String"},
			answer1: 
				{type: "String",
				lowercase: true },
			question2:
				{type: "String"},
			answer2:
				{type: "String",
				lowercase: true }
			},
		firstName:
		{
			type: "String",
			required: true,
		},
		middleName:
		{
			type:"String"
		},
		lastName:
		{
			type: "String",
			required: true,
		},
		billingAddress: {
			type:mongoose.SchemaTypes.ObjectId,
			ref:"Address"
		},
		shippingAddress: {
			type:mongoose.SchemaTypes.ObjectId,
			ref:"Address"}
		},
		{cart:
			{
			type:mongoose.SchemaTypes.ObjectId,
			ref:"cart"} }
		
	);

	const cartSchema = new Schema(
		{
			user_id:"String",	
			items:[{
				product_id:"String",
				sku_id:"String",
				quantity:"Number",
				price:"Number"
			}]		
		}
	)  
	const CartModel =model("Cart",cartSchema)
	const UsersModel = model("Users", usersSchema)
	const AddressModel = model("Address", addressSchema)

	module.exports = {CartModel,UsersModel,AddressModel}