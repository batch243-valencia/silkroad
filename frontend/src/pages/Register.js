import { Button, Col, Container, Dropdown, DropdownButton, Form, Row } from 'react-bootstrap';
import { Navigate,useHistory } from 'react-router-dom';
import { useEffect, useState,useContext } from 'react';
import Swal from 'sweetalert2';
import {UserContext} from '../UserContext';


export default function Register() {

const {        
        userId, setUserId,
        setToken
      } =useContext(UserContext);

const debugpopulate=(props)=>{
const debugOn= true
return (debugOn?props: null)
}

const [userName,            setUserName] = useState(debugpopulate("UserName")); 
const [password,            setPassword] = useState(debugpopulate("password")); 
const [email,               setEmail] = useState(debugpopulate("email@email.com")); 
const [contactNo,           setContactNo] = useState(debugpopulate("09752345678")); 

// const [question1,           setQuestion1] = useState('What was your mothers middle name'); 
const [question1, setQuestion1] = useState(debugpopulate("Who is your favorite band?"));
  function handleQuestion1Change(event) {
    console.log(event.target.value)
    setQuestion1(event.target.value);
  }
  const [question2, setQuestion2] = useState(debugpopulate("What is the name of your favorite pet?"));
  function handleQuestion2Change(event) {
    console.log(event.target.value)
    setQuestion2(event.target.value);
  }
const [answer1,             setAnswer1] = useState(debugpopulate("paramore")); 
// const [question2,           setQuestion2] = useState('What was your first pets name'); 
const [answer2,             setAnswer2] = useState(debugpopulate("elliot")); 

const [firstName,           setFirstName] = useState(debugpopulate("first name")); 
const [middleName,          setMiddleName] = useState(debugpopulate('')); 
const [lastName,            setLastName] = useState(debugpopulate("last Name")); 
const [isSeller,            setIsSeller] = useState(debugpopulate(true)); 
const [street,              setstreet] = useState(debugpopulate("street")); 
const [cityOrMunicipality,      setCityOrMunicipality] = useState(debugpopulate("city")); 
const [country,             setCountry] = useState(debugpopulate("Country")); 
// const [shippingAddress,     setShippingAddress] = useState('');


  const  handleSubmit = async  e => {
    
    e.preventDefault();
    const userDetails =await {
        userName,
        password,
        email,
        contactNo,
        passwordRetrieval:{
            "question1":question1,
            "answer1":answer2,
            "question2":question2,
            "answer2":answer2
        },
        isSeller,
        firstName,
        middleName,
        lastName,
        "billingAddress":{
            "street":street,
            "cityOrMunicipality": cityOrMunicipality,
            "country":country
        }
    }
    console.log(userDetails)
    const registeredUser =await RegisterUser(userDetails)
    console.log(registeredUser)
    const result = await registeredUser 
    console.log(registeredUser.userName)
    
if(registeredUser.status === 403){
    Swal.fire({
        title:"Warning Duplicate Entry Record",
        icon: "warning",
        text: "Username, Email, or Contact Number is already taken"
    })
    return false
}
if(registeredUser.status === 400){
    Swal.fire({
        title:"Something Went Wrong",
        icon: "error",
        text: "Please Look for Elliot to Fix this problem"
    })
    return false
}
if(registeredUser.status === 200){
    const result =  await loginUser({
      "userInput":userDetails.userName,
      "password":userDetails.password
    });
    
    localStorage.setItem('id', result.user._id)
    localStorage.setItem('token', result.token);
    localStorage.setItem('userName', result.user.userName);
    localStorage.setItem('isSeller', result.user.isSeller);
    setToken(result.token);
    setUserId(result.user._id);
    setUserName(result.user.userName);
    setIsSeller(result.user.isSeller);

    Swal.fire({
        title:`Welcome ${userDetails.userName}`,
        icon: "success",
        text: "You are now registered!"
    })
    // const navigate = useNavigate();
    return <Navigate to="/"/>
    
}
 }
  
  async function RegisterUser(UserInfo) {
    // console.log(credentials)
    const response = await fetch('http://localhost:4000/user/register', {
    method: 'POST',  
    mode:'cors',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
   },
   body: JSON.stringify(UserInfo)
 })
 return response
}
 
  const generateLabelInput=(controlId,label,getter,setter,inputType="text",required=true,disable=false)=>{
    return(
    <Form.Group className="mb-3 " controlId={controlId}>
        <Form.Label>{label}</Form.Label>
        <Form.Control type={inputType}
            required={required}
            placeholder={label}
            value={getter}
            disabled={disable}
            onChange={event => {
                setter(event.target.value)
            }}
        />
    </Form.Group>
    )
  }

  async function loginUser(credentials) {
    // console.log(credentials)
    const response = await fetch('http://localhost:4000/user/login', {
    method: 'POST',  
    mode:'cors',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
   },
//    body: JSON.stringify(credentials)
   body: JSON.stringify(credentials)
 })
 try {
  const result = await response.json()
  
  return result

 } catch (error) {
   return Swal.fire({
                    title:"Authentication Failed",
                    icon: "error",
                    text: "Wrong credentials, please try again!"
                })
 }
}
    return (
         <Container>
        {userId && <Navigate to="/"/>}
            <Row>
            
                <Col className='offset-4 mt-4' xs={12} md={6} lg={4}>
                    <Form className='bg-secondary p-3' onSubmit={handleSubmit}>
                        {/* username */}
                    {/* generateLabelInput(controlId,label,getter,setter,inputType) */}
                    {generateLabelInput("username","Username",userName,setUserName)}
                    {generateLabelInput("Password","Password",password,setPassword,"password")}
                    {generateLabelInput("Email","Email",email,setEmail,"email")}
                    {generateLabelInput("ContactNo","Contact Number",contactNo,setContactNo,"number")}

                    <Form.Group className="mb-3 " controlId={question1}>
                    <Form.Label>Question 1</Form.Label>
                    <Form.Select aria-label="Default select example"  value={question1} onChange={handleQuestion1Change}>
                        <option value="DEFAULT" hidden>Choose here</option>
                        <option>What year was your father (or mother) born?</option>
                        <option>What was your favorite food as a child?</option>
                        <option>Who is your favorite band?</option>
                        <option>Who is your favorite superhero?</option>
                    </Form.Select>
                    </Form.Group>
                    {/* {generateLabelInput("Question1","Question1",question1,setQuestion1,"text",false,true)} */}
                    {generateLabelInput("Answer1","Answer 1",answer1,setAnswer1)}
                    <Form.Group className="mb-3 " controlId={question2}>
                    <Form.Label>Question 2</Form.Label>
                    <Form.Select aria-label="Default select example"  value={question2} onChange={handleQuestion2Change}>
                        <option value="DEFAULT" hidden>Choose here</option>
                        <option>What is the name of your favorite pet?</option>
                        <option>In what city were you born?</option>
                        <option>What is your mother's maiden name?</option>
                        <option>What was your favorite food as a child?</option>
                    </Form.Select>
                    </Form.Group>
                    
                    {/* {generateLabelInput("Question2","Question 2",question2,setQuestion2,"text",false,true)} */}
                    {generateLabelInput("Answer2","Answer 2",answer2,setAnswer2)}

                    {generateLabelInput("FirstName","First Name",firstName,setFirstName)}
                    {generateLabelInput("MiddleName","Middle Name",middleName,setMiddleName,"text",false)}
                    {generateLabelInput("LastName","Last Name",lastName,setLastName)}

                    {generateLabelInput("Street","Street",street,setstreet)}
                    {generateLabelInput("CityOrMunicipality","City Or Municipality",cityOrMunicipality,setCityOrMunicipality,)}
                    {generateLabelInput("Country","Country",country,setCountry)}
                    
                    {/* {is seller*/} 
                    {/* {address*/} 
                    {/* {address*/} 
                        <div className='text-center'>
                            <Button className="btn-lg" variant="primary" type="submit">
                                Create Product
                            </Button>
                        </div>
                    </Form>
                </Col>
            </Row>
        </Container>
    )
};
