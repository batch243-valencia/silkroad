import { useState, useEffect, useContext } from 'react';
import {UserContext} from '../UserContext';
import Form from 'react-bootstrap/Form';
import { Navigate, useNavigate } from 'react-router-dom';


export default function Dashboard(params) {
    console.log(localStorage.getItem('isSeller'))
return (
    
    localStorage.getItem('isSeller')
    ?<Navigate to ="/admin/"/>
    :<Navigate to ="/user"/>
)    
};

