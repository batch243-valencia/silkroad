import { Navigate } from "react-router-dom"
import {UserContext} from '../UserContext';
import react,{useContext, useEffect,useState} from 'react'
import Swal from 'sweetalert2';

export default function Logout() {
    // localStorage.clear()
 const {unSetUserData} = useContext(UserContext);
// const parsedData = JSON.parse(userData)
// const [userName, setUserName] = useState(userData.userName)
// const [isSeller, setIsSeller] = useState(userData.isSeller)
    localStorage.removeItem('userData')
    localStorage.removeItem('token')
    
    useEffect(()=>{
        unSetUserData(null);
    })
    
    Swal.fire({
        title:"Logged Out",
        icon: "info"
    })
    return ( 
        <Navigate to ="/login"/>
     );
}
 
