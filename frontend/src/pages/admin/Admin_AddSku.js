import { Button, Col, Container, Form, Row } from "react-bootstrap"
import { useState, useEffect, useContext } from 'react';
import Swal from 'sweetalert2';
import { Navigate } from "react-router-dom";



    async function addProduct(productDetails) {    
        
        const id= localStorage.getItem('id')
        const token = localStorage.getItem('token');
        const data = productDetails;

        const response = await fetch(`http://localhost:4000/products/seller/${id}/create`, {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            //    body: JSON.stringify(credentials)
            body: JSON.stringify(data)
        })
        try {
            console.log(response)
            const result = await response.json()
            console.log(result)
            return Swal.fire({
                title: `New product is created`,
                icon: "success",
                text: `${productDetails.productName} has been created`
            })
            return result
        } catch (error) {
            
            console.log(error)
            return Swal.fire({
                title: "Authentication Failed",
                icon: "error",
                text: "Wrong credentials, please try again!"
            })
        }
    }

export default function Admin_Dashboard(params) {
    // const [success, setSuccess] = useState('');
    const [seller, setSeller] = useState('');
    const [bucket, setbucket] = useState('');
    const [prodName, setProdName] = useState('');
    const [price, setPrice] = useState('');
    const [description, setDescription] = useState('');
    const [category, setCategory] = useState('');
    const [type, setType] = useState('');
    const [material, setMaterial] = useState('');

//     useEffect(() => {
    
//   }, [success])

const  handleSubmit = async  e => {
    e.preventDefault();
    const id= await localStorage.getItem('id')
    // console.log(userInput,password)
    const result =  await addProduct({
      seller:id,
      bucket,
      "productName":prodName,
      description,
      category,
      type,
      material
    });
    
    console.log(result)
}
    return (
        <Container>
    
            <Row>

                <Col className='offset-4 mt-4' xs={12} md={6} lg={4}>
                    <Form className='bg-secondary p-3' onSubmit={handleSubmit}>
                        {/* seller */}
                        <Form.Group className="mb-3 " controlId="seller">
                            <Form.Label>{localStorage.getItem('userName')}</Form.Label>
                            <Form.Control type="text"
                                placeholder="seller"
                                value={localStorage.getItem('userName')}
                                onChange={event => {
                                    setSeller(event.target.value)
                                }}
                                hidden
                                required
                            />
                        </Form.Group>
                        {/*bucket*/}
                        <Form.Group className="mb-3 " controlId="bucketName">
                            <Form.Label>Bucket Name</Form.Label>
                            <Form.Control type="text"
                                placeholder="Product name"
                                value={bucket}
                                onChange={event => {
                                    setbucket(event.target.value)
                                }}
                                required
                            />
                        </Form.Group>
                        {/* Product Name */}
                        <Form.Group className="mb-3 " controlId="productName">
                            <Form.Label>Product Name</Form.Label>
                            <Form.Control type="text" placeholder="Product name" required value={prodName} onChange={event => {
                                setProdName(event.target.value)
                            }} />
                        </Form.Group>
                        {/* Description */}
                        <Form.Group className="mb-3 " controlId="description">
                            <Form.Label>Description</Form.Label>
                            <Form.Control as="textarea" row={3} placeholder="description" required value={description} onChange={event => {
                                setDescription(event.target.value)
                            }} />
                        </Form.Group>
                        {/* category */}
                        <Form.Group className="mb-3 " controlId="category">
                            <Form.Label>Category</Form.Label>
                            <Form.Control type="text" row={3} placeholder="category" required value={category} onChange={event => {
                                setCategory(event.target.value)
                            }} />
                        </Form.Group>
                        {/* type */}
                        <Form.Group className="mb-3 " controlId="type">
                            <Form.Label>Type</Form.Label>
                            <Form.Control type="text" row={3} placeholder="type" required value={type} onChange={event => {
                                setType(event.target.value)
                            }} />
                        </Form.Group>
                        {/* materials */}
                        <Form.Group className="mb-3 " controlId="materials">
                            <Form.Label>material</Form.Label>
                            <Form.Control type="text" placeholder="materials" required value={material} onChange={event => {
                                setMaterial(event.target.value)
                            }} />
                        </Form.Group>

                        <div className='text-center'>
                            <Button className="btn-lg" variant="primary" type="submit">
                                Create Product
                            </Button>
                        </div>
                    </Form>
                </Col>
            </Row>
        </Container>

    )
};
