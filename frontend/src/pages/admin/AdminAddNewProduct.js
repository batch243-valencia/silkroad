import { Button, Col, Container, Form, Row } from "react-bootstrap"
import { useEffect, useState } from 'react';
import FetchUtil from '../../util/FetchUtil';
import Swal from 'sweetalert2'
import { useNavigate } from 'react-router-dom';
export default function AdminAddNewProduct() {
    const navigate = useNavigate();

    const [sellerId, setSellerId] = useState('');
    const [seller, setSeller] = useState('');
    const [bucket, setbucket] = useState('');
    const [productName, setproductName] = useState('');
    const [description, setDescription] = useState('');
    const [category, setCategory] = useState('');
    const [type, setType] = useState('');
    const [materials, setMaterials] = useState('');

    const [productDetails, setProductDetails] = useState({});

    const [dataLoaded, setDataLoaded] = useState(false);

    const [status, setStatus] = useState(
        {
            submitting: false,
            success: false,
            error: false
        });
    useEffect(() => {
        setProductDetails({
            seller,
            bucket,
            productName,
            description,
            category,
            type,
            materials
        });
        setbucket(productName.replace(/[aeiou ]/gi, ''))

        // console.log(seller,bucket,productName,description,category,type,materials)
        // console.log(productDetails)
    }, [seller, bucket, productName, description, category, type, materials]);

    useEffect(() => {
        let parsedUserData = localStorage.getItem("userData")
        if (typeof parsedUserData === "string") {
            parsedUserData = JSON.parse(parsedUserData);
            setDataLoaded(true)
            // console.log(parsedUserData)
            setSeller(parsedUserData.userName)
            setSellerId(parsedUserData._id)
        }
        // console.log(parsedUserData.userName)
        // console.log(parsedUserData)
        // console.log(typeof(parsedUserData))
        // console.log(seller)
    }, [])


    const handleSubmit = async (event) => {
        event.preventDefault();
        console.log("handleSubmit")
        // eslint-disable-next-line no-use-before-define
        setStatus({ ...status, submitting: true })
        console.log(productDetails)
        // const id = await localStorage.getItem('id');
        const token = await localStorage.getItem('token');
        /* Simple FetchUtility -FetchUtil(
                    url = '',
                    method="GET",
                    data={},
                    token)
                    */
        try {
            console.log("try")
            const response = await FetchUtil(`${process.env.REACT_APP_API_URL}/products/seller/${sellerId}/create`
                , "POST",
                productDetails,
                token)
            console.log(response.status)

            if (response.status === 403) {
                return Swal.fire({
                    title: 'Unable to add New product!',
                    text: 'Please Login to Seller',
                    icon: 'info',
                    confirmButtonText: 'OK'
                })
            }
            await Swal.fire({
                title: 'Success!',
                text: 'The product was added successfully.',
                icon: 'success',
                confirmButtonText: 'OK'
            })
            setStatus({
                submitting: false,
                success: true,
                error: false
            })
            console.log(`${process.env.REACT_APP_API_URL}/admin/products`)
            // navigate(`${process.env.REACT_APP_API_URL}/admin/products`);
            console.log(process.env)
            navigate('../../../../admin/products');
            // window.location.href('http://localhost:4000/admin/products')
            
        } catch (error) {
            console.log("error")
            console.log(error.message)
            if (error.status === 400 || error.status === 409) {
                await Swal.fire({
                    title: 'Error!',
                    text: "A product with the same name already exists",
                    icon: 'error',
                    confirmButtonText: 'OK'
                });

                setStatus({
                    submitting: false,
                    success: false,
                    error: true
                })
            } else {
                await Swal.fire({
                    title: 'Error!',
                    text: error.message,
                    icon: 'error',
                    confirmButtonText: 'OK'
                });
                setStatus({
                    submitting: false,
                    success: false,
                    error: true
                })
            }


        }
    }

    return (
        <Container>
            <Row>
                <Col className='offset-4 mt-4' xs={12} md={6} lg={4}>
                    <Form className='bg-secondary p-3' onSubmit={handleSubmit}>
                        {/* seller */}
                        <Form.Group className="mb-3 " controlId="seller">
                            <Form.Label><h1>{seller} - {bucket}</h1></Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="seller"
                                value={seller}
                                onChange={event => {
                                    setSeller(event.target.value)
                                }}
                                required
                                hidden
                            />
                        </Form.Group>
                        {/*bucket*/}
                        <Form.Group className="mb-3 " controlId="bucketName" hidden>
                            <Form.Label>Bucket Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Product name"
                                value={bucket}
                                onChange={event => {
                                    setbucket(event.target.value)
                                }}
                                required

                                disabled={status.submitting}
                            />
                        </Form.Group>
                        {/* Product Name */}
                        <Form.Group className="mb-3 " controlId="productName">
                            <Form.Label>Product Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Product name"
                                value={productName}
                                onChange={event => {
                                    setproductName(event.target.value)
                                }}
                                required
                                disabled={status.submitting}
                            />
                        </Form.Group>
                        {/* Description */}
                        <Form.Group className="mb-3 " controlId="description">
                            <Form.Label>Description</Form.Label>
                            <Form.Control
                                as="textarea"
                                placeholder="description"
                                value={description}
                                onChange={event => {
                                    setDescription(event.target.value.split('\n'))
                                }}
                                required
                                disabled={status.submitting}
                            />
                        </Form.Group>
                        {/* category */}
                        <Form.Group className="mb-3 " controlId="category">
                            <Form.Label>Category</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="category"
                                value={category}
                                onChange={event => {
                                    setCategory(event.target.value)
                                }}
                                required
                                disabled={status.submitting}
                            />
                        </Form.Group>
                        {/* type */}
                        <Form.Group className="mb-3 " controlId="type">
                            <Form.Label>Type</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="type"
                                value={type}
                                onChange={event => {
                                    setType(event.target.value)
                                }}
                                required
                                disabled={status.submitting}
                            />
                        </Form.Group>
                        {/* materials */}
                        <Form.Group className="mb-3 " controlId="materials">
                            <Form.Label>Materials</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="materials"
                                value={materials}
                                onChange={event => {
                                    setMaterials(event.target.value.split('\n'))
                                }}
                                required
                                disabled={status.submitting}
                            />
                        </Form.Group>

                        <div className='text-center'>
                            <Button
                                className="btn-lg"
                                variant="primary"
                                type="submit"
                                disabled={status.submitting}
                                onClick={handleSubmit}>
                                Create Product
                            </Button>
                        </div>
                    </Form>
                </Col>
            </Row>
        </Container>

    )
};
