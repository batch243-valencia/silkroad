import { useCallback, useState, useEffect, useContext } from 'react';
import { Button, Form, Col, Row, Container, Modal } from 'react-bootstrap';
import { Navigate, redirect, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { UserContext } from '../../UserContext';
import { Link } from "react-router-dom";
import { Table, Spinner } from 'react-bootstrap';
// import { useNavigate } from 'react-router';
import { ToggleButton, ToggleButtonGroup } from 'react-bootstrap';
import FetchUtil from '../../util/FetchUtil';


export default function Admin_Products() {

    const [show, setShow] = useState(false);
    const [sellerId, setSellerId] = useState('');
    const [seller, setSeller] = useState('');
    const [status, setStatus] = useState(
        {
            submitting: false,
            success: false,
            error: false
        });


    const navigate = useNavigate();
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [IsProductEmpty, setIsProductEmpty] = useState(false)

    // EDITHOOKS
    const [bucketId, setBucketId] = useState('')
    const [bucket, setBucket] = useState('')
    const [productName, setProductName] = useState('')
    const [description, setDescription] = useState('')
    const [category, setCategory] = useState('')
    const [type, setType] = useState('')
    const [materials, setMaterials] = useState('')
    const [mode, setMode] = useState('')

    async function showEdit(bucketValue, mode) {
        const { _id, bucket, productName, description, category, type, materials } = bucketValue
        console.log(bucketValue)
        setMode(mode)
        setShow(true);
        setBucketId(_id)
        setBucket(bucket)
        setProductName(productName)
        setDescription(description)
        setCategory(category)
        setType(type)
        setMaterials(materials)
    }

    const handleClose = useCallback(() => {
        setShow(false);
        setMode("")
        setShow("");
        setBucketId("")
        setBucket("")
        setProductName("")
        setDescription("")
        setCategory("")
        setType("")
        setMaterials("")
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [show]);

    const handleSave  = async() => {
        setLoading(true)
    console.log("save")
    // setShow(false);
 
    // console.log(productDetails)
    const method = "PATCH"
    const url= `${process.env.REACT_APP_API_URL}/products/seller/update/${bucketId}`
    const data = {
        bucket: bucket,
        productName: productName,
        description: description,
        category: category,
        type: type,
        materials: materials,
    }   
    const token = await localStorage.getItem('token');
    // console.log(url,method,data,token)
    console.log(token)
    try {
        const fetchPost = await FetchUtil(url,method,data,token)
        const postResult = fetchPost.data
        console.log(fetchPost.data)
        console.log(postResult)
    // console.log(postResult.status)
    setShow(false);
    setLoading(false)
    return postResult
} catch (error) {
    console.log(error)
    setShow(false);
    setLoading(false)
    return false
}
   
 
};
    useEffect(() => {
        const userData = JSON.parse(localStorage.getItem('userData'));
        const sellerId = userData._id;
        // console.log(`http://localhost:4000/products/seller/${sellerId}/`)
        async function fetchData() {
            try {
                const response = await fetch(`${process.env.REACT_APP_API_URL}/products/seller/${sellerId}`);
                // console.log(response)
                const data = await response.json();
                // console.log(data.length)
                // console.log(data)
                setData(data);
                // console.log(data)
                setLoading(false);
                setIsProductEmpty(false)
                // console.log(response.length)
            } catch (error) {
                setLoading(false);
                setIsProductEmpty(true)
            }
        }
        fetchData();
    }, [loading]);
    const handleSubmit = async (event) => {
        event.preventDefault();
        console.log("handleSubmit")
    }
    return (
        <div>
            {loading ? (
                <Spinner animation="border" variant="primary" />
            ) : (<>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>{localStorage.getItem('userName')}</th>
                            <th>Bucket</th>
                            <th>Product Name</th>
                            <th>Description</th>
                            <th>Category</th>
                            <th>Type</th>
                            <th>Materials</th>
                        </tr>
                    </thead>
                    <tbody>
                        {IsProductEmpty
                            ? <tr><td colSpan={7} className="text-center">No Product Found</td></tr>
                            : null}
                        {data.map((fetchData) => (
                            <tr key={fetchData._id}>
                                <td>{fetchData._id}</td>
                                <td>{fetchData.bucket}</td>
                                <td>{fetchData.productName}</td>
                                <td>{Array.isArray(fetchData.description) ? fetchData.description.join(', ') : fetchData.description}</td>
                                <td>{fetchData.category}</td>
                                <td>{fetchData.type}</td>
                                <td>{Array.isArray(fetchData.materials) ? fetchData.materials.join(', ') : fetchData.materials}</td>
                                <td><Button variant="primary" onClick={() => navigate
                                    (`skus/${fetchData.seller}/${fetchData._id}`)}>View</Button>{' '}</td>
                                <td><Button variant="secondary"
                                    onClick={() => { showEdit(fetchData, "Edit") }}
                                >Edit</Button>{' '}</td>
                            </tr>
                        ))}
                    </tbody>
                </Table>

                <Modal show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                        {/* <Modal.Title>{mode}</Modal.Title> */}
                    </Modal.Header>
                    <Modal.Body>
                        <Form className='bg-secondary p-3' onSubmit={handleSubmit}>
                            {/* seller */}
                            <Form.Group className="mb-3 " controlId="seller">
                                <Form.Label><h1>{mode}</h1></Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="seller"
                                    value={seller}
                                    onChange={event => {
                                        setSeller(event.target.value)
                                    }}
                                    required
                                    hidden
                                />
                            </Form.Group>
                            <Form.Group className="mb-3 " controlId="bucketId">
                                <Form.Label>bucketId</Form.Label>
                                <Form.Control
                                    className="text-muted"
                                    type="text"
                                    placeholder="bucketId"
                                    value={bucketId}
                                    onChange={event => {
                                        setBucketId(event.target.value)
                                    }}
                                    required
                                    disabled
                                    muted
                                />
                            </Form.Group>
                            {/*bucket*/}
                            <Form.Group className="mb-3 " controlId="bucketName">
                                <Form.Label>Bucket Name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Product name"
                                    value={bucket}
                                    onChange={event => {
                                        setBucket(event.target.value)
                                    }}
                                    required

                                    disabled={status.submitting}
                                />
                            </Form.Group>

                            {/* Product Name */}
                            <Form.Group className="mb-3 " controlId="productName">
                                <Form.Label>Product Name</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Product name"
                                    value={productName}
                                    onChange={event => {
                                        setProductName(event.target.value)
                                    }}
                                    required
                                    disabled={status.submitting}
                                />
                            </Form.Group>
                            {/* Description */}
                            <Form.Group className="mb-3 " controlId="description">
                                <Form.Label>Description</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    placeholder="description"
                                    value={description}
                                    onChange={event => {
                                        setDescription(event.target.value.split('\n'))
                                    }}
                                    required
                                    disabled={status.submitting}
                                />
                            </Form.Group>
                            {/* category */}
                            <Form.Group className="mb-3 " controlId="category">
                                <Form.Label>Category</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="category"
                                    value={category}
                                    onChange={event => {
                                        setCategory(event.target.value)
                                    }}
                                    required
                                    disabled={status.submitting}
                                />
                            </Form.Group>
                            {/* type */}
                            <Form.Group className="mb-3 " controlId="type">
                                <Form.Label>Type</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="type"
                                    value={type}
                                    onChange={event => {
                                        setType(event.target.value)
                                    }}
                                    required
                                    disabled={status.submitting}
                                />
                            </Form.Group>
                            {/* materials */}
                            <Form.Group className="mb-3 " controlId="materials">
                                <Form.Label>Materials</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="materials"
                                    value={materials}
                                    onChange={event => {
                                        setMaterials(event.target.value.split('\n'))
                                    }}
                                    required
                                    disabled={status.submitting}
                                />
                            </Form.Group>


                        </Form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary"
                            onClick={handleClose}
                        >
                            Close
                        </Button>
                        <Button variant="primary"
                            onClick={handleSave}
                        >
                            Save Changes
                        </Button>

                    </Modal.Footer>
                </Modal>

            </>

            )}


        </div>
    )
}
