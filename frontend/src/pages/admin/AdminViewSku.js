import FetchUtil from "../../util/FetchUtil"
import { useParams } from 'react-router-dom';
import { useContext, useEffect, useState } from "react";
import {userContext} from "../../UserContext"

export default function AdminViewSku() {
    // :seller/:prodid/:sku
    const {seller,prodId,sku } = useParams();
    const [response, setResponse] = useState([])
    const [error, setError] = useState(null)

    // const navigate = useNavigate();
        const [data, setData] = useState('');
        const [loading, setLoading] = useState(true);
        const [IsProductEmpty,setIsProductEmpty] = useState(false)
   useEffect(() => {
            const userData=localStorage.getItem('userData')
            console.log(userData)
            const {_id : sellerId} = JSON.parse(userData)
            console.log(sellerId)
            // console.log(`http://localhost:4000/products/seller/${sellerId}/`)
            async function fetchData() {
                try {
                    const response = await fetch(`${process.env.REACT_APP_API_URL}/products/sku/${seller}/${prodId}/${sku}`);
                    const result = await response.json();
                    // console.log(data.length)
                    console.log(result)
                    setData(result);
                    
                    console.log(data.length)
                    console.log(data)
                    if(result.length===0){setIsProductEmpty(false)
                    return }
                    setLoading(false);
                    
                } catch (error) {
                    setLoading(false);
                    setIsProductEmpty(true)
                }
            }
            fetchData();
        }, []);
     return(
        <>
        {error && <p>{error}</p>}
        {data && Array.isArray(data) ? 
        <table className="table">
            <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Price</th>
                    <th>Stocks</th>
                    <th>Color</th>
                    <th>Size</th>
                    <th>Weight</th>
                    <th>isActive</th>
                    <th>createdOn</th>
                </tr>
            </thead>
            <tbody>
                {data.map((product) => (
                    <tr key={product._id}>
                        <td>{product.price}</td>
                        <td>{product.stocks}</td>
                        <td>{product.color}</td>
                        <td>{product.size}</td>
                        <td>{product.weight}</td>
                        <td>{product.isActive}</td>
                        <td>{product.createdOn}</td>
                    </tr>
                ))}
            </tbody>
        </table> 
        : <div>{response ? "No products found" : "Loading..."}</div>}
        </>
    )
}