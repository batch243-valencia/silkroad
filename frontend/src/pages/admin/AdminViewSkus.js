import FetchUtil from "../../util/FetchUtil"
import { useParams, useSearchParams } from 'react-router-dom';
import { useContext, useEffect, useState } from "react";
import {userContext} from "../../UserContext"
import BootstrapSwitchButton from 'bootstrap-switch-button-react'
import { Button } from "react-bootstrap";

export default function AdminViewSkus() {
    // :seller/:prodid/:sku
    
    const [response, setResponse] = useState([])
    const [error, setError] = useState(null)
    const {seller,prodId} = useParams();
    

    // const navigate = useNavigate();
        const [data, setData] = useState('');
        const [prodData, setprodData] = useState('');
        const [loading, setLoading] = useState(true);
        const [IsProductEmpty,setIsProductEmpty] = useState(false)
    




    const handleIsActiveToggle  = async(skuId,isActive) => {
        console.log(seller,prodId)
        console.log(isActive)
    setLoading(true)
    console.log("save")
    const method = "PATCH"
    const url= await`${process.env.REACT_APP_API_URL}/products/sku/update/${skuId}`
    const data = { isActive: !isActive }; 
    const token = await localStorage.getItem('token');
    // console.log(url,method,data,token)
    console.log(token)
    try {
        const fetchPost = await FetchUtil(url,method,data,token)
        const postResult = fetchPost.data
        console.log(fetchPost.data)
        console.log(postResult)
    // console.log(postResult.status)
    
    setLoading(false)
    return postResult
} catch (error) {
    console.log(error)
    
    setLoading(false)
    return false
}}
   

const handleAddSku = async(test)=>{
    
}
   useEffect(() => {
            const userData=localStorage.getItem('userData')
            // console.log(userData)
            const {_id : sellerId} = JSON.parse(userData)
            // console.log(sellerId)
            // console.log(`http://localhost:4000/products/seller/${sellerId}/`)
            async function fetchData() {
                try {
                    const url =`${process.env.REACT_APP_API_URL}/products/get/${prodId}`
                    const method = "GET"
                    const data = {}; 
                    const token = await localStorage.getItem('token');
                    const bucket = await fetch(url, {
                    method: method, // *GET, POST, PUT, DELETE, etc.
                    mode: 'cors', // no-cors, *cors, same-origin
                    // cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                    // credentials: 'same-origin', // include, *same-origin, omit
                    headers: {
                        'Content-Type': 'application/json',
                        // 'Content-Type': 'application/x-www-form-urlencoded',
                        'Authorization': `Bearer ${token}`
                    },
                    redirect: 'follow', // manual, *follow, error
                    // referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
                    // body: JSON.stringify(data) // body data type must match "Content-Type" header
                    });
                    
                    
                    // console.log(bucket)
                    const bucketResult = await bucket.json();
                    // console.dir(bucketResult)
                    // console.log(`${process.env.REACT_APP_API_URL}/products/skus/${seller}/${prodId}/`)
                    const response = await fetch(`${process.env.REACT_APP_API_URL}/products/skus/${seller}/${prodId}/`);
                    const result = await response.json();
                    
                    // console.log(result)
                    setData(result);
                    setprodData({productName: `${bucketResult.productName}` , _id: bucketResult._id})
                    // console.log(prodData)
                    if(result.length===0){setIsProductEmpty(true)
                    return }
                    setLoading(false);
                } catch (error) {
                    setLoading(false);
                    setIsProductEmpty(true)
                }
            }
            fetchData();
        }, [loading]);
     return(
        <>
        {error && <p>{error}</p>}
        {data && Array.isArray(data) ? <>
        <table className="table">
            <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Price</th>
                    <th>Stocks</th>
                    <th>Color</th>
                    <th>Size</th>
                    <th>Weight</th>
                    <th>isActive</th>
                    <th>createdOn</th>
                </tr>
            </thead>
            <tbody>
                {data.map((product) => (
                    <tr key={product._id}>
                        <td>{`${prodData.productName} ${product.color} ${product.size}`}</td>
                        <td>{product.price}</td>
                        <td>{product.stocks}</td>
                        <td>{product.color}</td>
                        <td>{product.size}</td>
                        <td>{product.weight}</td>
                        <td><BootstrapSwitchButton
                            checked={product.isActive}
                            onChange={() => handleIsActiveToggle(product._id,product.isActive)}
                            />
                        </td>
                        <td>{product.createdOn}</td>
                        <td><Button variant="secondary">Edit</Button>{' '}</td>
                        
                         
                    </tr>
                ))}
            </tbody>
            
        </table> <Button variant="success" onClick={() => handleAddSku()}>Add</Button>{' '}</>
        
        : <div>{response ? "No products found" : "Loading..."}</div>}
        </>
    )
}