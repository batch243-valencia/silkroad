import { useState, useEffect, useContext } from 'react';
import {Button, Form, Col, Row, Container} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import {UserContext} from '../UserContext';
import swalFire from '../util/swalFire'
import FetchUtil from '../util/FetchUtil';


let baseURL = "http://localhost:4000"

export default function Login() {
  
  const navigate = useNavigate();
  const [isLoading, setIsLoading] = useState(false);
  const {userData, setUserData} = useContext(UserContext);
  const [formData, setFormData] = useState({userInput: '', password: ''});
  const [userInput, setuserInput] = useState('');
  const [password, setPassword] = useState('');
  
  useEffect(() => {
    setFormData({userInput: userInput, password: password});     
  }, [userInput, password]);
  
  const handleSubmit = async (event) => {
    // console.log("localstorage test");
    // localStorage.setItem("test", "tressssssssssssssstttt")
    // console.log(localStorage.getItem("test"))
    event.preventDefault();
    setIsLoading(true);
    try { 
      const response = await FetchUtil(`${baseURL}/user/login`,"POST", formData);
      if (!response.ok){
        throw new Error("User Not Found");}
      if (response.ok){
        const result = await response.json();
        delete result.user.password;
        console.log(result.token)
        console.log(result.user)
        swalFire(`Welcome ${result.user.userName}!`,'success','You are now Registered');
        // console.log(localStorage.getItem('userData'))
        // console.log(result.user)
        // localStorage.setItem('userData', JSON.stringify(result.user, null, 2));
        localStorage.setItem('userData', JSON.stringify(result.user));
        localStorage.setItem('token', result.token);
        setUserData(localStorage.getItem('userData'));
        navigate('/home');
    }
    setIsLoading(false);
    } catch (error) {
      console.log(error)
      setIsLoading(false);
      swalFire('Error', 'error', 'esssrror');
    }
  }
    
  return (
    (userData)
      ? <Navigate to ="/"/>
       :<>
          <Form onSubmit={handleSubmit}>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>Email or UserName</Form.Label>
                  <Form.Control type="text" placeholder="Enter Username, Email or Phone Number"
                   onChange={e => setuserInput(e.target.value)}
                   disabled={isLoading}
                   required/>
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicPassword">
                  <Form.Label>Password</Form.Label>
                  <Form.Control type="password" placeholder="Password"
                  onChange={e => setPassword(e.target.value)}
                  disabled={isLoading}
                  required
                  />
              </Form.Group>
              <Button variant="primary" type="submit" disabled={isLoading}>
                  Submit
              </Button>
          </Form>
        </>
  )
}
